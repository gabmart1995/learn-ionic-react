export interface Component {
    icon: string,
    name: string,
    redirectTo: string
};