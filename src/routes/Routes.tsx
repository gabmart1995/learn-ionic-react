import Home from '../pages/Home';
import Alert from '../pages/alert/Alert';
import ActionSheet from '../pages/action-sheet/ActionSheet';
import Avatar from '../pages/avatar/Avatar';
import Buttons from '../pages/buttons/Buttons';
import Cards from '../pages/cards/Cards';
import Checkbox from '../pages/checkbox/Checkbox';
import DateTime from '../pages/datetime/Datetime';
import Fab from '../pages/fab/Fab';
import Grid from '../pages/grid/Grid';
import InfiniteScroll from '../pages/infinite/InfiniteScroll';
import Inputs from '../pages/inputs/Inputs';
import ListsPage from '../pages/lists/Lists';
import ListReorder from '../pages/lists/ListsReorder';
import Loading from '../pages/loading/Loading';
import ModalPage from '../pages/modals/Modals';
import PopoverPage from '../pages/popover/PopoverPage';
import Progress from '../pages/progress/Progress_range';
import Refresher from '../pages/refresher/Refresher';
import SearchBar from '../pages/searchbar/searchBar';
import Segment from '../pages/segment/Segment';
import Slides from '../pages/slides/Slides';
import Tabs from '../pages/tabs/Tabs';
import Toasts from '../pages/toasts/Toasts';

export interface Route {
    path: string,
    component: any
}

export const Routes: Route[] = [
    {
        path: '/home',
        component: Home
    },
    {
        path: '/alert',
        component: Alert
    },
    {
        path: '/action-sheet',
        component: ActionSheet
    },
    {
        path: '/avatar',
        component: Avatar
    },
    {
        path: '/buttons',
        component: Buttons
    },
    {
        path: '/cards',
        component: Cards
    },
    {
        path: '/checkbox',
        component: Checkbox
    },
    {
        path: '/datetime',
        component: DateTime
    },
    {
        path: '/fab',
        component: Fab
    },
    {
        path: '/grid',
        component: Grid
    },
    {
        path: '/infinite-scroll',
        component: InfiniteScroll
    },
    {
        path: '/inputs',
        component: Inputs
    },
    {
        path: '/lists',
        component: ListsPage
    },
    {
        path: '/list-reorder',
        component: ListReorder
    },
    {
        path: '/loading',
        component: Loading
    },
    {
        path: '/modal',
        component: ModalPage
    },
    {
        path: '/popover',
        component: PopoverPage
    },
    {
      path: '/progress',
      component: Progress
    },
    {
      path: '/refresher',
      component: Refresher
    },
    {
      path: '/search',
      component: SearchBar
    },
    {
      path: '/segment',
      component: Segment
    },
    {
      path: '/slides',
      component: Slides
    },
    // router tabs
    {
      path: '/tabs/account',
      component: Tabs
    },
    {
      path: '/tabs/seetings',
      component: Tabs
    },
    {
      path: '/tabs/contact',
      component: Tabs
    },
    {
      path: '/toasts',
      component: Toasts
    }
];
