import React from 'react';
import { IonPage, IonContent, IonButton, useIonToast } from '@ionic/react';

import Header from '../../components/header/Header';

const Toasts: React.FC = () => {

  const [ present, dismiss ] = useIonToast();

  function showToast() {
    present( 'Mi primer toast de ionic', 2000 )
  }

  function showToastOptions() {
    present({
      buttons: [{
        text: 'Aceptar',
        handler: () => dismiss()
      }],
      message: 'Toast con opciones para el usuario',
      onDidDismiss: () => console.log('toast cerrado'),
      onWillDismiss: () => console.log('toast inicio cerrandose'),
      duration: 2000
    })
  }

  return (
    <IonPage>
      <Header title="toasts" />
      <IonContent>
        <IonButton expand="block" onClick={ showToast }>Toast simple</IonButton>
        <IonButton expand="block" onClick={ showToastOptions }>Toast con opciones</IonButton>
      </IonContent>
    </IonPage>
  );
}

export default Toasts;
