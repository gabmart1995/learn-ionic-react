import React from 'react';
import { IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle, IonContent, IonIcon, IonItem, IonPage } from '@ionic/react';
import Header from '../../components/header/Header';
import { pinOutline } from 'ionicons/icons';

const Cards: React.FC = () => {

  return (
    <IonPage>
      <Header title="cards" />
      <IonContent>

        <IonCard mode="ios">
          <IonCardHeader>
            <IonCardSubtitle>Card Subtitle</IonCardSubtitle>
            <IonCardTitle>Card Title</IonCardTitle>
          </IonCardHeader>

          <IonCardContent>
            Keep close to Nature's heart... and break clear away, once in awhile,
            and climb a mountain or spend a week in the woods. Wash your spirit clean.
          </IonCardContent>
        </IonCard>

        <IonCard>
          <IonItem color="primary">
            <IonIcon icon={pinOutline} slot="start"></IonIcon>
            Marcador
          </IonItem>
          <IonCardContent>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit magnam, nisi delectus et voluptas mollitia doloribus reprehenderit quibusdam eveniet velit ducimus alias, inventore aspernatur voluptates numquam dolorum, fuga earum quidem?
          </IonCardContent>
        </IonCard>

        <IonCard>
          <img src="https://dummyimage.com/600x400/000/fff" alt="image-dummy" />
          <IonCardContent>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit magnam, nisi delectus et voluptas mollitia doloribus reprehenderit quibusdam eveniet velit ducimus alias, inventore aspernatur voluptates numquam dolorum, fuga earum quidem?
          </IonCardContent>
        </IonCard>

      </IonContent>
    </IonPage>    
  );
}

export default Cards;