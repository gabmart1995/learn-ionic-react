
import React from 'react';
import { IonButtons, IonContent, IonHeader, IonMenuButton, IonPage, IonTitle, IonToolbar } from '@ionic/react';


// componentes
import Lists from '../components/lists/Lists';

// estilos
import './Home.css';


const Home: React.FC = () => {

  return (
    <IonPage>

      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton menu="first" color="primary"></IonMenuButton>
          </IonButtons>
          <IonTitle>Componentes</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen>
        <Lists />
      </IonContent>

    </IonPage>
  );
};

export default Home;
