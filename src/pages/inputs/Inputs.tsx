import React, { useState } from 'react';
import { IonButton, IonContent, IonInput, IonItem, IonLabel, IonList, IonListHeader, IonPage, IonText } from '@ionic/react';

import { FormikProps, Form, withFormik, FormikErrors } from 'formik';

import Header from '../../components/header/Header';
import './inputs.css';

interface FormValues {
  nombre: string,
  correo: string,
  password: string
}

// model
const getUser: () => FormValues = () => ({
  nombre: '',
  correo: '',
  password: ''
});

// se utiliza el hook de formik para crear una nueva instancia del formulario

// primero se crea el componente del formulario
const FormUser = ( props: FormikProps<FormValues> ) => {

  const { values, errors, handleChange, isValid, dirty } = props;

  // console.log( props );

  return (
    <Form>
        <IonList>
          <IonListHeader>
            <IonLabel>Input normales</IonLabel>
          </IonListHeader>
          <IonItem>
            <IonLabel position="floating">Nombre:</IonLabel>
            <IonInput
              name="nombre"
              onIonChange={ handleChange }
              value={ values.nombre }
              type="text"
              placeholder="Nombre ..."
            />
          </IonItem>
          { errors.nombre && dirty && (
              <div className="validation">
                <IonText color="danger">{ errors.nombre }</IonText>
              </div>
            )
          }
        </IonList>

        <IonListHeader>
          <IonLabel>Formulario valido: { isValid ? 'Válido' : 'Inválido' }</IonLabel>
        </IonListHeader>

        <IonItem>
          <IonLabel position="floating">Correo:</IonLabel>
          <IonInput
            value={ values.correo }
            onIonChange={ handleChange }
            type="email"
            name="correo"
            placeholder="Correo ..."
          />
        </IonItem>
        { errors.correo && dirty && (
            <div className="validation">
              <IonText color="danger">{ errors.correo }</IonText>
            </div>
          )
        }

        <IonItem>
          <IonLabel position="floating">Contraseña:</IonLabel>
          <IonInput
            onIonChange={ handleChange }
            type="password"
            value={ values.password }
            name="password"
            placeholder="Contraseña ..."
          />
        </IonItem>
        { errors.password && dirty && (
            <div className="validation">
              <IonText color="danger">{ errors.password }</IonText>
            </div>
          )
        }

        <IonButton disabled={ !isValid } type="submit" expand="block" className="ion-margin-top">
          Enviar
        </IonButton>

      </Form>
  );
}

// segundo se crea la funcion de validacion de los campos
function validateForm( values: FormValues ): FormikErrors<FormValues> {

  let errors: FormikErrors<FormValues> = {};

  const rexp = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;

  // validacion nombre
  if ( !values.nombre ) {
    errors.nombre = 'Campo requerido';

  } else if ( values.nombre.length > 0 && values.nombre.length < 3 ) {
    errors.nombre = 'Mínimo 2 caracteres';

  }

  // validacion correo
  if ( !values.correo ) {
    errors.correo = 'Campo requerido';

  } else if ( !rexp.test( values.correo ) ) {
    errors.correo = 'Dirección de correo ínvalida';

  }

  // validaciones password
  if ( !values.password ) {
    errors.password = 'Campo requerido';

  } else if ( values.password.length > 0 && values.password.length < 8 ) {
    errors.password = 'Mínimo 8 caracteres';
  }

  return errors;
}

// tercero se crea  el objeto de configurcion para que formik cree el hook y el componente
// el primer valor de la definicion de la interface son los props del componente de formik
// insertado de forma personalizada

// retorna un decorador
const FormComponent = withFormik<{ formInitial: FormValues }, FormValues>({

  mapPropsToValues: () => {
    return {
      nombre: '',
      correo: '',
      password: ''
    }
  },

  // valida al cambio
  validateOnChange: true,
  validateOnMount: true,
  validate: validateForm,

  handleSubmit: ( values ) => {
    console.log( values );
  }

})( FormUser );
// la segunda ejecucion en FormUser permite crear un componente funcional que contiene las propiedades del hook

// cuarto se utiliza el componente
const Inputs: React.FC = () => {
  return (
    <IonPage>
      <Header title="inputs" />
      <IonContent>
        <FormComponent formInitial={ getUser() } />
      </IonContent>
    </IonPage>
  );
}

export default Inputs;
