import React from 'react';
import { IonPage, IonContent, IonGrid, IonRow, IonCol, IonCard, IonImg, IonCardHeader, IonCardSubtitle, IonCardContent } from '@ionic/react';
import Header from '../../components/header/Header';
import Stan from '../avatar/stan-lee.jpg'

const Grid: React.FC = () => {
  
  
  function getColumn(): React.ReactNode[] {
    
    let nodes: React.ReactNode[] = [];

    for ( let i = 1; i <= 12; i++ ) {
      nodes.push(
        <IonCol key={ i } sizeLg="3" sizeMd="4" sizeSm="6" size="12">
          <IonCard>
            <IonImg alt="stan-lee" src={ Stan }></IonImg>
            <IonCardSubtitle>Awesome subtitle</IonCardSubtitle>
            <IonCardHeader>Stan Lee</IonCardHeader>
            <IonCardContent>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorem dolor incidunt voluptates. Minus fugiat aliquid at accusamus ullam dolorum officiis velit necessitatibus molestiae, voluptate officia magnam laudantium consectetur, animi aliquam.
            </IonCardContent>
          </IonCard>
        </IonCol>
      );
    }

    return nodes;
  }
  
  return (
    <IonPage>
      <Header title="grid" />
      <IonContent>
        <IonGrid>
          <IonRow>
            { getColumn() }
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
}

export default Grid;