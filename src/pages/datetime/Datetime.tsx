import React from 'react';
import { IonContent, IonDatetime, IonItem, IonLabel, IonList, IonListHeader, IonPage } from '@ionic/react';
import Header from '../../components/header/Header';

const DateTime: React.FC = () => {
    
  let fechaNacimento = new Date();
  
  // parametros de configuracion
  const customYearValues = [2025, 2020, 2016, 2008, 2004, 2000, 1996];
  const customPickerOptions = {
    buttons: [
      { text: 'Hola', handler: ( $event: any ) => console.log( $event ) },
      { text: 'Mundo', handler: ( $event: any ) => console.log( 'log!!' ) },
    ]
  };

  function handleChangeDate( $event: any ) {
    
    // console.log( $event );
    // se recupera la fecha en JS

    console.log( new Date( $event.target.value ) );
  }
  
  return (
    <IonPage>
      <Header title="datetime" />
      <IonContent>
          <IonList>
            
            <IonListHeader>
              <IonLabel>Datos de Nacimiento</IonLabel>
            </IonListHeader>

            <IonItem>
              <IonLabel>Fecha de Nacimiento</IonLabel>
              <IonDatetime 
                value={ fechaNacimento.toISOString() }
                displayFormat="DD MMMM YYYY"
                onIonChange={ handleChangeDate }
              ></IonDatetime>
            </IonItem>

            <IonListHeader>
              <IonLabel>Restricciones</IonLabel>
            </IonListHeader>

            <IonItem>
              <IonLabel>Min y Máx</IonLabel>
              <IonDatetime 
                displayFormat="DD MMMM YYYY"
                min="2015-01-01"
                max="2025-12-31"
              ></IonDatetime>
            </IonItem>

            {/* Años */}
            <IonItem>
              <IonLabel>Años</IonLabel>
              <IonDatetime 
                displayFormat="YYYY"
                min="2015"
                max="2025"
              ></IonDatetime>
            </IonItem>

            {/* Opciones personalizadas */}
            <IonItem>
              <IonLabel>Opciones personalizadas</IonLabel>
              <IonDatetime 
                yearValues={ customYearValues } 
                pickerOptions={ customPickerOptions }
              ></IonDatetime>
            </IonItem>

          </IonList>
      </IonContent>
    </IonPage>
  );
}

export default DateTime;