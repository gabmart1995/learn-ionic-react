import { IonBackButton, IonButton, IonButtons, IonContent, IonFooter, IonHeader, IonIcon, IonPage,  IonPopover,  IonTitle, IonToolbar } from '@ionic/react';
import { personOutline } from 'ionicons/icons';
import React, { useState } from 'react';
import PopoverInfo from '../../components/popover/Popover';

const PopoverPage: React.FC = () => {

  const [ popover, setPopover ] = useState({
    ev: undefined,
    popover: false
  });

  function showPopover( $event?: any ) {
    setPopover({ ev: $event, popover: true });
  }

  function closePopover() {
    setPopover({ ...popover, popover: false });
  }

  function showData( $event: any ) {
    const { data } = $event.detail;

    console.log( data );
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton defaultHref="/" color="primary"></IonBackButton>
          </IonButtons>
          <IonTitle>Popovers</IonTitle>
          <IonButtons slot="end">
            <IonButton color="primary" onClick={ ( $event: any ) => showPopover( $event ) }>
              <IonIcon slot="icon-only" icon={ personOutline }></IonIcon>
            </IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>

      <IonContent className="ion-padding">
        <IonPopover event={ popover.ev } onWillDismiss={ showData } backdropDismiss={ false }
          onDidDismiss={ closePopover } isOpen={ popover.popover }>
          <PopoverInfo />
        </IonPopover>
        <IonButton onClick={ () => showPopover() } expand="block">Mostrar popover</IonButton>
      </IonContent>

      <IonFooter>
        <IonToolbar>
          <IonButtons slot="start">
            <IonButton color="primary" onClick={ ( $event: any ) => showPopover( $event ) }>
              <IonIcon slot="icon-only" icon={ personOutline }></IonIcon>
            </IonButton>
          </IonButtons>
          <IonTitle>Footer</IonTitle>
        </IonToolbar>
      </IonFooter>
    </IonPage>
  );
}

export default PopoverPage;
