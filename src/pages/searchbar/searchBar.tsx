import React, { useState } from 'react';
import { IonList, IonItem, IonLabel, IonPage, IonHeader, IonSearchbar, IonTitle, IonToolbar, IonButtons, IonBackButton, IonContent, useIonViewDidEnter } from '@ionic/react';
import { SearchbarChangeEventDetail } from '@ionic/core';

import { fetchApi } from '../../services/FetchApi';
import { searchFilter } from '../../services/SearchFilter';

interface album {
  title: string,
  id: number,
  userid: number
};

const SearchBar: React.FC = () => {

  const base = fetchApi('https://jsonplaceholder.typicode.com');
  const albumResource = base('/albums');

  const [ albums, setAlbums ] = useState<Array<any>>([]);

  useIonViewDidEnter(() => {
    getAlbums();
  });

  async function getAlbums() {

    try {
      let response: album[] = await albumResource.get();
      setAlbums( response );

    } catch ( error ) {
      console.error( error );

    }
  }

  function handleChange( $event: CustomEvent<SearchbarChangeEventDetail> ): void {

    searchFilter({
      searchValue: $event.detail.value,
      property: 'title',
      array: albums,
      callback: ( array: any[] | undefined ) => {

        if ( !array ) {
          getAlbums();
        } else {
          setAlbums( array );
        }

      }
    });
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton defaultHref="/" color="primary" />
          </IonButtons>
          <IonTitle>SearchBar</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent>
        <IonSearchbar
          placeholder="Buscar algo ..."
          animated
          debounce={ 250 }
          onIonChange={ handleChange }
          inputmode="text"
        ></IonSearchbar>

        <IonList>
          { albums.map(( album: album ) => (
              <IonItem key={ album.id }>
                <IonLabel>{ album.title }</IonLabel>
              </IonItem>
            ))
          }
        </IonList>

      </IonContent>
    </IonPage>
  );
}

export default SearchBar;
