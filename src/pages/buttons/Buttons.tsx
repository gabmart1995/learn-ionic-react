import React, { useState } from 'react';
import { IonPage, IonContent, IonButton, IonBackButton, IonButtons, IonHeader, IonTitle, IonToolbar, IonIcon } from '@ionic/react';
import { heart, heartOutline, star } from 'ionicons/icons';

const Buttons: React.FC = () => {
  
  const [ favorite, setfavorite ] = useState( false );
  
  function handleClick(): void {
    setfavorite( !favorite );
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton color="primary" defaultHref="/"></IonBackButton>
          </IonButtons>
          <IonTitle>Buttons</IonTitle>  
          <IonButtons slot="end">
            <IonButton onClick={ handleClick }>
              <IonIcon icon={ !favorite ? heartOutline : heart } color="danger" 
                slot="icon-only"></IonIcon>
            </IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding">

        <h3>Default</h3> 
        <IonButton>Default</IonButton>

        <h3>Anchor</h3>
        <IonButton href="#">Anchor</IonButton>

        <h3>Colors</h3>
        <IonButton color="primary">Primary</IonButton>
        <IonButton color="secondary">Secondary</IonButton>
        <IonButton color="tertiary">Tertiary</IonButton>
        <IonButton color="success">Success</IonButton>
        <IonButton color="warning">Warning</IonButton>
        <IonButton color="danger">Danger</IonButton>
        <IonButton color="light">Light</IonButton>
        <IonButton color="medium">Medium</IonButton>
        <IonButton color="dark">Dark</IonButton>

        <h3>Expand</h3>
        <IonButton expand="full" color="primary">Full Button</IonButton>
        <IonButton expand="block" color="primary">Block Button</IonButton>

        <h3>Round</h3>
        <IonButton shape="round">Round Button</IonButton>

        <h3>Fill</h3>        
        <IonButton expand="full" fill="outline">Outline + Full</IonButton>
        <IonButton expand="block" fill="outline">Outline + Block</IonButton>
        <IonButton shape="round" fill="outline">Outline + Round</IonButton>

        <h3>Icons</h3>
        <IonButton>
          <IonIcon slot="start" icon={star} />
          Left Icon
        </IonButton>

        <IonButton>
          Right Icon
          <IonIcon slot="end" icon={star} />
        </IonButton>

        <IonButton>
          <IonIcon slot="icon-only" icon={star} />
        </IonButton>

        <h3>Sizes</h3>
        <IonButton size="large">Large</IonButton>
        <IonButton>Default</IonButton>
        <IonButton size="small">Small</IonButton>
  
      </IonContent>
    </IonPage>
  );
} 

export default Buttons;