import React from 'react';
import { useHistory } from 'react-router-dom';
import { IonCardHeader, IonCardContent, IonCardSubtitle, IonCard, IonPage, IonContent, IonSlides, IonSlide, IonButton } from '@ionic/react';

import Photos from './slides-svg/photos.svg';
import Music from './slides-svg/music-player-2.svg';
import Calendar from './slides-svg/calendar.svg';
import Placeholder from './slides-svg/placeholder-1.svg';

import './Slides.css';

const Slides: React.FC = () => {

  /*
    existen 2 formas de redireccion

    1.- usar el hook o el prop de history para obtener una referencia del objeto
    history a traves del metodo push redirecciona al sitio indicado.

    2.- Ejecutar en la vista el component REdirect combinado con un state
    que redireccione cuando aplica el cambio.
  */

  let history = useHistory();

  const slides: { img: string, titulo: string, desc: string }[] = [
    {
      img: Photos,
      titulo: 'Comparte Fotos',
      desc: 'Mira y comparte increíbles fotos de todo el mundo'
    },
    {
      img: Music,
      titulo: 'Escucha Música',
      desc: 'Toda tu música favorita está aquí'
    },
    {
      img: Calendar,
      titulo: 'Nunca olvides nada',
      desc: 'El mejor calendario del mundo a tu disposición'
    },
    {
      img: Placeholder,
      titulo: 'Tu ubicación',
      desc: 'Siempre sabremos donde estás!'
    }
  ];

  function getSlides() {

    let nodes: React.ReactNode[] = [];

    for ( let i = 0; i < slides.length; i++ ) {
      nodes.push(
        <IonSlide key={ i }>
          <IonCard mode="ios">
            <img src={ slides[i].img } className="slide-image" />
            <IonCardHeader>
              <IonCardSubtitle>{ slides[i].titulo }</IonCardSubtitle>
            </IonCardHeader>
            <IonCardContent>{ slides[i].desc }</IonCardContent>
          </IonCard>
        </IonSlide>
      );

      if ( i === ( slides.length - 1 ) ) {
        nodes.push(
          <IonSlide key={ i + 1 }>
            <IonButton onClick={ handleClick } expand="block" fill="clear">Comenzar</IonButton>
          </IonSlide>
        );
      }
    }

    return nodes;
  }

  function handleClick() {
    // aplica la redireccion;
    history.goBack();
  }

  return (
    <IonPage>
      <IonContent>
        <IonSlides pager={ true } mode="ios" className="slide-full">
          { getSlides() }
        </IonSlides>
      </IonContent>
    </IonPage>
  );
}

export default Slides;
