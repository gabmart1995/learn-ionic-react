import React, { useState } from 'react';

import { IonList, IonIcon, IonItem, IonRange, IonPage, IonContent, IonProgressBar } from '@ionic/react';
import Header from '../../components/header/Header';

import { sunny } from 'ionicons/icons';

const Progress: React.FC = () => {

  const [ porcen, setPorcen ] = useState( 0.05 );

  function rangeChange( $event: any ) {

    let { value } = $event.detail;

    value /= 100;

    console.log( value );

    setPorcen( value );
  }

  return (
    <IonPage>
      <Header title="Progress" />
      <IonContent>
        <IonProgressBar type="determinate" value={ porcen } color="primary" />

        <IonList>
          <IonItem>
            <IonIcon slot="start" size="small" icon={ sunny }></IonIcon>
            <IonRange min={ 0 } max={ 100 } onIonChange={ rangeChange }></IonRange>
            <IonIcon slot="end" icon={ sunny }></IonIcon>
          </IonItem>
        </IonList>

      </IonContent>
    </IonPage>
  );
}

export default Progress;
