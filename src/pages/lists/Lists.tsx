import React from 'react';
import { withIonLifeCycle, IonContent, IonIcon, IonItem, IonItemOption, IonItemOptions, IonItemSliding, IonLabel, IonList, IonLoading, IonPage } from '@ionic/react';
import Header from '../../components/header/Header';
import { heartOutline, shareOutline, trashOutline } from 'ionicons/icons';

import { fetchApi } from '../../services/FetchApi';

interface listState {
  users: any[],
  loading: boolean
}

class ListsPage extends React.Component<{}, listState> {

  baseUrl = fetchApi('https://jsonplaceholder.typicode.com');
  usersResource = this.baseUrl('/users');

  // se crea la objeto de referencia
  list = React.createRef<HTMLIonListElement>();

  constructor( props = {} ) {
    super( props );

    this.state = {
      users: [],
      loading: false
    };
  }

  ionViewDidEnter() {

    this.setState(( state: listState ) => {
      return { ...state, loading: true };
    })

    const getUsers = async () => {

      try {
        let users = await this.usersResource.get();

        this.setState({ users, loading: false });

      } catch ( error ) {

        console.error( error );

        this.setState({ ...this.state, loading: false });
      }

    }

    getUsers();
  }

  favorite( user: any ): void {
    console.log( user );

    this.list.current?.closeSlidingItems();
  }

  share( user: any ): void {
    console.log( user );

    this.list.current?.closeSlidingItems();
  }

  delete( user: any ): void {
    console.log('delete', user.name );

    this.list.current?.closeSlidingItems();
  }

  renderUsers() {
    return this.state.users.map(( user: any ) => (

      <IonItemSliding key={ user.id }>

        <IonItemOptions side="start">
          <IonItemOption onClick={ () => this.favorite( user ) }>
            <IonIcon slot="icon-only" icon={ heartOutline }></IonIcon>
          </IonItemOption>
          <IonItemOption color="dark" onClick={ () => this.share( user ) }>
            <IonIcon slot="icon-only" icon={ shareOutline }></IonIcon>
          </IonItemOption>
        </IonItemOptions>

        <IonItem>
          <IonLabel>
            <h3>{ user.name }</h3>
            <p>{ user.email }</p>
          </IonLabel>
          <IonLabel slot="end" className="ion-text-end">
            { user.phone }
          </IonLabel>
        </IonItem>

        <IonItemOptions side="end">
          <IonItemOption color="danger" onClick={ () => this.delete( user ) }>
            <IonIcon slot="icon-only" icon={ trashOutline }></IonIcon>
          </IonItemOption>
        </IonItemOptions>

      </IonItemSliding>
    ));
  }

  render() {

    return (
      <IonPage>
        <Header title="Lists" />
        <IonContent>
          <IonLoading isOpen={ this.state.loading } message="cargando ..."></IonLoading>
          <IonList ref={ this.list }>
            { this.renderUsers() }
          </IonList>
        </IonContent>
      </IonPage>
    );
  }
}


export default withIonLifeCycle( ListsPage );
