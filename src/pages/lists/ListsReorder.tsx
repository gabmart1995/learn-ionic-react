import { IonBackButton, IonButtons, IonContent, IonHeader, IonLabel, IonList, IonPage, IonTitle, IonToolbar, IonItem, IonReorderGroup, IonReorder, IonButton, IonIcon } from '@ionic/react';
import { pizza } from 'ionicons/icons';
import React, { useState } from 'react';

const ListReorder: React.FC = () => {
   
  const [ reorder, setReorder ] = useState( true );

  let caracters: string[] = ['Aquaman', 'Superman', 'Batman', 'Flash', 'Wonder woman'];
  
  function doReorder( $event: any ) {
    
    const { from, to } = $event.detail;

    const itemMove = caracters.splice( from, 1 )[0];
    
    caracters.splice( to, 0, itemMove );
    
    console.log({ from, to, itemMove, caracters });
    
    $event.detail.complete();
  }

  return (
    <IonPage>
      <IonHeader>

        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton color="primary" defaultHref="/"></IonBackButton>
          </IonButtons>
          <IonTitle>List - Reorder</IonTitle>
          <IonButtons slot="end">
            <IonButton onClick={ () => setReorder( !reorder ) }>Toggle</IonButton>
          </IonButtons>
        </IonToolbar>

      </IonHeader>
      <IonContent>
        
        {/*  listas */}
        <IonList>
          <IonReorderGroup disabled={ reorder } onIonItemReorder={ doReorder }>
            { caracters.map(( caracter: string, index: number ) => (
                <IonItem key={ index }>
                  <IonLabel>{ caracter }</IonLabel>
                  <IonReorder slot="end">
                    <IonIcon icon={ pizza }></IonIcon>
                  </IonReorder>
                </IonItem>
              )) 
            }
          </IonReorderGroup>
        </IonList>

      </IonContent>
    </IonPage>
  );
}

export default ListReorder;