import React, { useState } from 'react';
import { IonContent, IonPage, IonButton, IonAlert, AlertButton, AlertInput } from '@ionic/react';
import Header from '../../components/header/Header';
// import ExploreContainer from '../components/ExploreContainer'; 
import './Alert.css';

const Alert: React.FC = () => {
  
  // states
  const [ showAlert, setShowAlert ] = useState( false );
  const [ showAlert2, setShowAlert2 ] = useState( false );
  const [ showPrompt, setShowPrompt ] = useState( false );

  const buttons: AlertButton[] = [
    { text: 'OK'  }
  ]

  const buttons2: AlertButton[] = [
    { text: 'Cancelar', role: 'Cancel', cssClass: 'red' },
    { text: 'OK', handler: () => console.log('click en ok') },
  ];

  const buttons3: AlertButton[] = [
    { text: 'Cancelar', role: 'Cancel', cssClass: 'red' },
    { text: 'OK', handler: ( data: any ) => console.log( data ) },
  ];

  const inputsPrompt: AlertInput[] = [
    {
      name: 'name1',
      type: 'text',
      placeholder: 'Placeholder 1'
    },
    {
      name: 'name2',
      type: 'text',
      id: 'name2-id',
      value: 'hello',
      placeholder: 'Placeholder 2'
    },
    {
      name: 'name3',
      value: 'http://ionicframework.com',
      type: 'url',
      placeholder: 'Favorite site ever'
    },
    // input date with min & max
    {
      name: 'name4',
      type: 'date',
      min: '2017-03-01',
      max: '2018-01-12'
    },
    // input date without min nor max
    {
      name: 'name5',
      type: 'date'
    },
    {
      name: 'name6',
      type: 'number',
      min: -5,
      max: 10
    },
    {
      name: 'name7',
      type: 'number'
    },
    {
      name: 'name8',
      type: 'password',
      placeholder: 'Advanced Attributes',
      attributes: {
        maxlength: 4,
        inputmode: 'decimal'
      }
    }
  ]; 

  return (
    <IonPage>
      <Header title="alert" />
      <IonContent fullscreen className="ion-padding">
        <IonButton expand="block" onClick={ () => setShowAlert( true ) }>
          Alerta
        </IonButton>
        <IonButton color="success" expand="block" onClick={ () => setShowAlert2( true ) }>
          Alerta Multibotones
        </IonButton>

        <IonButton color="dark" expand="block" onClick={ () => setShowPrompt( true ) }>
          Alerta Prompt
        </IonButton>

        <IonAlert 
          isOpen={ showAlert }
          header="Alerta"
          backdropDismiss={ false }
          subHeader="Subtitulo"
          message="Este es el mensaje" 
          buttons={ buttons } 
          onDidDismiss={ () => setShowAlert( false ) }
        ></IonAlert>

        <IonAlert 
          isOpen={ showAlert2 }
          header="Alerta2"
          backdropDismiss={ false }
          subHeader="Subtitulo"
          message="Este es el mensaje" 
          buttons={ buttons2 } 
          onDidDismiss={ () => setShowAlert2( false ) }
        ></IonAlert>

        {/* Alert prompt */ }
      
        <IonAlert
          isOpen={ showPrompt }
          header="Prompt!!"
          backdropDismiss={ false }
          inputs={ inputsPrompt }
          buttons={ buttons3 }
          onDidDismiss={ () => setShowPrompt( false ) }
        ></IonAlert>
        
      </IonContent>
    </IonPage>
  );
};

export default Alert;