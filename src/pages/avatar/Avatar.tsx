import React from 'react';
import { IonPage, IonImg, IonContent, IonToolbar, IonHeader, IonTitle, IonChip, IonButtons, IonBackButton, IonAvatar, IonLabel, IonList, IonItem } from '@ionic/react';
import Stan from './stan-lee.jpg';

function getChips(): React.ReactNode[] {
  
  let chips: React.ReactNode[] = [];

  for ( let i = 1; i <= 5; i++ ) {

    chips.push(
      <IonChip key={ i } color="primary" outline={ true }>
        <IonAvatar>
          <IonImg src={ Stan } alt="stan-lee"></IonImg>
        </IonAvatar>
        <IonLabel>Default</IonLabel>
      </IonChip>
    );
  }

  return chips;
}

function getItems(): React.ReactNode[] {
  
  let items: React.ReactNode[] = [];

  for ( let i = 1; i <= 10; i++ ) {

    items.push(
      <IonItem key={ i }>
        <IonAvatar slot="start">
          <IonImg src={ Stan } alt="stan-lee"></IonImg>
        </IonAvatar>  
        <IonLabel>Stan Lee</IonLabel>
      </IonItem>
    );
  }

  return items;
}

const Avatar: React.FC = () => {

  return (
    <IonPage>
      
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton color="primary" defaultHref="/"></IonBackButton>
          </IonButtons>
          <IonTitle>Avatar</IonTitle>
          <IonAvatar slot="end">
            <IonImg src={ Stan } alt="stan-lee"></IonImg>
          </IonAvatar>  
        </IonToolbar>
      </IonHeader>
      
      <IonContent fullscreen>        
        { getChips() }
        <IonList>
          { getItems() }
        </IonList>
      </IonContent>
      
    </IonPage>
  );
}

export default Avatar;