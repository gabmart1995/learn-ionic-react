import React from 'react';
import { IonPage, IonContent, IonFooter, IonTitle, IonList, IonItem, IonLabel, IonFab, IonFabButton, IonIcon, IonFabList } from '@ionic/react';

import Header from '../../components/header/Header';
import { add, addOutline, logoFacebook, logoGithub, logoGoogle, logoTwitter, logoVimeo, logoYoutube } from 'ionicons/icons';

const Fab: React.FC = () => {
  
  function getData(): React.ReactNode {
    
    let childs: React.ReactNode[] = [];

    for ( let i = 1; i <= 100; i++ ) {
      childs.push(
        <IonItem key={ i }>
          <IonLabel>Item { i }</IonLabel>
        </IonItem>
      );
    }

    return childs;
  }

  return (
    <IonPage>
      <Header title="fabs" />
      <IonContent>
        
        {/* 
          <IonFab vertical="top" horizontal="end" slot="fixed">
            <IonFabButton>
              <IonIcon icon={ add }></IonIcon>
            </IonFabButton>
          </IonFab> 
        */}
        
        <IonFab vertical="bottom" horizontal="end" slot="fixed">
          <IonFabButton>
            <IonIcon icon={ addOutline }></IonIcon>
          </IonFabButton>

          <IonFabList side="top">
            <IonFabButton color="facebook">
              <IonIcon  icon={ logoFacebook }></IonIcon>
            </IonFabButton>
            <IonFabButton color="twitter">
              <IonIcon icon={ logoTwitter }></IonIcon>
            </IonFabButton>
            <IonFabButton color="youtube">
              <IonIcon icon={ logoYoutube }></IonIcon>
            </IonFabButton>
          </IonFabList>

          <IonFabList side="start">
            <IonFabButton color="vimeo">
              <IonIcon icon={ logoVimeo }></IonIcon>
            </IonFabButton>
            <IonFabButton color="google">
              <IonIcon icon={ logoGoogle }></IonIcon>
            </IonFabButton>
            <IonFabButton color="github">
              <IonIcon icon={ logoGithub }></IonIcon>
            </IonFabButton>
          </IonFabList>

        </IonFab>

        <IonList>
          { getData() }
        </IonList>
      </IonContent>
      {/* 
        <IonFooter>
          <IonTitle>Footer</IonTitle>
        </IonFooter> 
      */}
    </IonPage>
  );
}

export default Fab;