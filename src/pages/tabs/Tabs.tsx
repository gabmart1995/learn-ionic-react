import React from 'react';
import { Route, Redirect, useHistory, Switch } from 'react-router-dom';
import { IonReactRouter } from '@ionic/react-router';
import { IonIcon, IonTabs, IonTabBar, IonTabButton, IonLabel, IonRouterOutlet } from '@ionic/react';
import { personOutline, callOutline, cogOutline } from 'ionicons/icons';

import Avatar from '../avatar/Avatar';
import InfiniteScroll from '../infinite/InfiniteScroll';
import Lists from '../lists/Lists';

const Tabs: React.FC = () => {

  return (
    <>
      <IonTabs>
        <IonRouterOutlet>
          <Route path="/tabs/account" exact component={ Avatar } />
          <Route path="/tabs/contact" exact component={ Lists } />
          <Route path="/tabs/seetings" exact component={ InfiniteScroll } />
        </IonRouterOutlet>

        <IonTabBar slot="bottom">
          <IonTabButton tab="/tabs/account"  href="/tabs/account">
            <IonIcon icon={ personOutline }></IonIcon>
            <IonLabel>Account</IonLabel>
          </IonTabButton>

          <IonTabButton tab="/tabs/contact" href="/tabs/contact">
            <IonIcon icon={ callOutline }></IonIcon>
            <IonLabel>Contacts</IonLabel>
          </IonTabButton>

          <IonTabButton tab="/tabs/seetings" href="/tabs/seetings">
            <IonIcon icon={ cogOutline }></IonIcon>
            <IonLabel>Seetings</IonLabel>
          </IonTabButton>
        </IonTabBar>
      </IonTabs>
    </>
  );
}

export default Tabs;
