import React, { useState } from 'react';
import { RefresherEventDetail } from '@ionic/core';
import { IonPage, IonItem, IonLabel, IonRefresher, IonRefresherContent, IonList, IonContent } from '@ionic/react';

import Header from '../../components/header/Header';

import 'animate.css';

const Refresher: React.FC = () => {

  const [ items, setItems ] = useState<Array<any>>([]);

  function doRefresh( $event: CustomEvent<RefresherEventDetail> ) {

    // console.log( $event );

    setTimeout(() => {

      setItems( new Array(20).fill( null ));

      $event.detail.complete();

    }, 2500);
  }

  return (
    <IonPage>
      <Header title="Refresher" />
      <IonContent>

        {/* Ion refresher */}
        <IonRefresher slot="fixed" onIonRefresh={ doRefresh }>
          <IonRefresherContent></IonRefresherContent>
        </IonRefresher>

        <IonList>
          { items.map(( value: null, index: number ) => (
              <IonItem className="animate_animated animate__fadeInDown animate__slow" key={ index }>
                <IonLabel>{ index + 1 }</IonLabel>
              </IonItem>
            ))
          }
        </IonList>
      </IonContent>
    </IonPage>
  );
}

export default Refresher;
