import { IonButton, IonContent, IonPage, useIonModal } from '@ionic/react';
import React from 'react';

import Header from '../../components/header/Header';

const ModalPage: React.FC = () => {
  
  function showModal() {
    present();
  }
  
  function hideModal( data: any ) {
    
    if ( data ) {
      console.log( data );
    }
    
    dismiss();
  }
  
  // el primer parametro es el componente hijo del modal
  // el segundo es el objeto de las props
  const [ present, dismiss ] = useIonModal( ModalContent, {
    onDismiss: hideModal,
    name: 'Gabriel',
    country: 'Venezuela'
  });


  return (
    <IonPage>
      <Header title="Modal" />
      <IonContent className="ion-padding">
        <IonButton onClick={ showModal } expand="block">Mostrar modal</IonButton>
      </IonContent>
    </IonPage>
  );
}

const ModalContent: React.FC<{ 
  onDismiss: ( data?: any ) => void, 
  name: string, 
  country: string 
}> = ( props ) => {
  
  console.log( props );

  const { onDismiss } = props;

  return (
    <IonContent className="ion-padding">
      <IonButton expand="block" onClick={ () => onDismiss() }>Salir sin argumentos</IonButton>
      <IonButton expand="block" onClick={ () => onDismiss({ name: 'Felipe', country: 'España' }) }>
        Salir con argumentos
      </IonButton>
    </IonContent>
  );
}

export default ModalPage;