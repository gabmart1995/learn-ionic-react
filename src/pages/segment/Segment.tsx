import React, { useState } from 'react';

import { IonHeader, IonBackButton, useIonViewDidEnter, IonSkeletonText, IonToolbar, IonButtons, IonTitle, IonList, IonItem, IonSegment, IonSegmentButton, IonLabel, IonPage, IonContent } from '@ionic/react';
import { SegmentChangeEventDetail } from '@ionic/core';

import SuperHeroes from './superheroes.json';

import { searchFilter } from '../../services/SearchFilter';

const Segment: React.FC = () => {

  // tipado
  type Comic = 'DC Comics' | 'Marvel Comics' | 'todos';

  const [ superHeroes, setSuperHeroes ] = useState<Array<any>>([]);
  const [ option, setOption ] = useState<Comic>('todos');

  // skeleton text
  let skeleton = new Array(10).fill( null );

  useIonViewDidEnter(() => {
    setTimeout( () => setSuperHeroes( SuperHeroes ), 1500 );
  });

  function handleChange( $event: CustomEvent<SegmentChangeEventDetail> ) {

    if ( $event.detail.value as Comic === 'todos' ) {
      setOption('todos');
      setSuperHeroes( SuperHeroes );
      return;
    }

    searchFilter({
      searchValue: $event.detail.value! as Comic,
      array: SuperHeroes,
      property: 'publisher',
      callback: ( result: any[] | undefined ) => {
        setOption( $event.detail.value! as Comic );
        setSuperHeroes( result! );
      }
    });
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
          <IonBackButton color="primary" defaultHref="/"></IonBackButton>
          </IonButtons>

          <IonTitle>Segment</IonTitle>

        </IonToolbar>

        <IonSegment value={ option } onIonChange={ handleChange }>
          <IonSegmentButton value="todos">
            <IonLabel>Todos</IonLabel>
          </IonSegmentButton>
          <IonSegmentButton value="DC Comics">
            <IonLabel>DC Comics</IonLabel>
          </IonSegmentButton>
          <IonSegmentButton value="Marvel Comics">
            <IonLabel>Marvel Comics</IonLabel>
          </IonSegmentButton>
        </IonSegment>

      </IonHeader>
      <IonContent>

        <IonList>
          { superHeroes.length > 0 && superHeroes.map(( superHeroe: any, index: number ) => (
              <IonItem key={ index }>
                <IonLabel>
                  <h3>{ superHeroe.superhero } <small>{ superHeroe.alter_ego }</small></h3>
                  <p>{ superHeroe.first_appearance }</p>
                </IonLabel>
                <IonLabel slot="end" className="ion-text-right">{ superHeroe.publisher }</IonLabel>
              </IonItem>
            ))
          }

          {/* skeleton text */}
          { superHeroes.length === 0 && skeleton.map(( sk: null, index: number ) => (
              <IonItem key={ index }>
                <IonLabel>
                  <h3>
                    <IonSkeletonText animated style={{ width: '70%' }}></IonSkeletonText>
                  </h3>
                  <p>
                    <IonSkeletonText animated style={{ width: '100%' }}></IonSkeletonText>
                  </p>
                </IonLabel>
                <IonSkeletonText slot="end" style={{ width: '50px', height: '50px' }} animated></IonSkeletonText>
              </IonItem>
            ))
          }
        </IonList>

      </IonContent>
    </IonPage>
  );
}

export default Segment;
