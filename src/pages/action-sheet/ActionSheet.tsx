import React, { useState } from 'react';
import { IonContent, IonPage, IonButton, ActionSheetOptions, ActionSheetButton, IonActionSheet } from '@ionic/react'; 

import Header from '../../components/header/Header';

// icons
import { trashOutline, discOutline, caretForwardCircleOutline, heart, closeOutline } from 'ionicons/icons';

import './ActionSheet.css';

// hook action sheet
const ActionSheet: React.FC = () => {
  
  const [ showActionSheet, setShowActionSheet ] = useState( false );

  const buttons : ActionSheetButton[] = [
    { text: 'Delete', role: 'destructive', cssClass: 'red',  icon: trashOutline, handler: () => console.log('delete clicked') },
    { text: 'Share', icon: discOutline, handler: () => console.log('share clicked') },
    { text: 'Play (open Modal)', icon: caretForwardCircleOutline, handler: () => console.log('Play clicked') },
    { text: 'Favorite', icon: heart, handler: () => console.log('Favorite clicked') },
    { text: 'Cancel', icon: closeOutline, role: 'cancel', handler: () => console.log('Cancel clicked') }
  ];
 
  return (
    <IonPage>
      <Header title="action sheet" />
      <IonContent fullscreen className="ion-padding">
        <IonButton expand="block" onClick={ () => setShowActionSheet( true ) }>
          Mostrar action-sheet
        </IonButton>
        <IonActionSheet 
          isOpen={ showActionSheet } 
          header="Albumes" 
          buttons={ buttons }
          backdropDismiss={ false }
          onDidDismiss={ () => setShowActionSheet( false ) }  
        ></IonActionSheet>
      </IonContent>
    </IonPage>
  );
};

export default ActionSheet;