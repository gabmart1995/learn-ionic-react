import { IonContent, IonInfiniteScroll, IonInfiniteScrollContent, IonItem, IonLabel, IonList, IonPage, IonText } from '@ionic/react';
import React from 'react';
import Header from '../../components/header/Header';

interface state { 
  disabledInfiniteScroll: boolean, 
  items: React.ReactNode[]  
}

class InfiniteScroll extends React.Component<{}, state>  {

  constructor( props: {} ) {
    
    super( props );
    
    this.state = {
      disabledInfiniteScroll: false,
      items: []
    };

    this.loadData = this.loadData.bind( this );
  }

  componentDidMount() {
    this.setData();
  }

  setData( limit: number = 0, itemsAdd: number = 20 ) {
    
    let newArray: React.ReactNode[] = [];

    for ( let i = limit; i < limit + itemsAdd; i++ ) {
      newArray.push(
        <IonItem key={ i + 1 }>
           <IonLabel>Item { i + 1 }</IonLabel> 
        </IonItem>
      );
    }

    this.setState(( state: state, props: {} ) => {
      return { ...state, items: state.items.concat( newArray ) };
    });
  }

  loadData( $event: any ) {
  
    setTimeout(() => {

      if ( this.state.items.length < 50 ) {

        // toma la longitud actual y le añade 10 asi sucesivamente hasta que llega a 50
        this.setData( this.state.items.length, 10 );
        
        console.log('añadiendo ...', this.state.items );
  
        $event.target.complete();
                
        return;
      }
      
      $event.target.complete();

      this.setState(( state: state ) => {
        return { ...state, disabledInfiniteScroll: true  };
      });
      
      console.log('completado.');
      
    }, 1500);
  }

  render() {
    return (
      <IonPage>
        <Header title="infinite scroll" />
        <IonContent>
          <IonList>
            { this.state.items }
          </IonList>

          {/* infinite scroll */}
          <IonInfiniteScroll  
            threshold="100px" 
            disabled={ this.state.disabledInfiniteScroll }
            onIonInfinite={ this.loadData }
          >
            <IonInfiniteScrollContent
              loadingSpinner="crescent"
              loadingText="cargando items ..."
            ></IonInfiniteScrollContent>
          </IonInfiniteScroll>
        </IonContent>
      </IonPage>      
    );
  }
}

export default InfiniteScroll;