import React, { useState } from 'react';

import { IonButton, IonCheckbox, IonContent, IonItem, IonLabel, IonList, IonPage } from '@ionic/react';
import Header from '../../components/header/Header';

interface Check {
  name: string,
  selected: boolean
}

const Checkbox: React.FC = () => {
  
  const data: Check[] = [
    { name: 'primary', selected: false },
    { name: 'secondary', selected: true },
    { name: 'dark', selected: false },
    { name: 'success', selected: true }
  ];
  
  // para ver el cambio
  /* function handleChange( check: Check, actualValue: boolean ): void {
    
    check.selected = actualValue;
    
    console.log( check );
  } */
  
  // documentacion para el manejo del estado
  const [ checks, setChecks ] = useState( data );

  function handleChange( index: number,  actualValue: boolean ) {
    
    const newState: Check[] = checks.map(( check, i ) => {
      
      if ( index === i ) {
        return { ...check, selected: actualValue };
      }

      return check;
    });

    // la actualizacion del estado es asincrona, no consultes inmediatamente
    // despues de la funcion setChecks, porque retorna el valor antes del
    // cambio

    setChecks( newState ); 
  }

  function seeData() {
    console.log( checks );
  }
  
  return (
    <IonPage>
      <Header title="checkbox" />
      <IonContent>
        <IonList className="mb-3">
          {
            checks.map(( check: Check, index: number ) => (
              
              <IonItem key={ index }>
                <IonLabel>{ check.name }</IonLabel>
                <IonCheckbox 
                  color={ check.name } 
                  checked={ check.selected } 
                  slot="end"
                  onIonChange={ ( $event: any ) => handleChange( index, $event.target.checked ) }
                ></IonCheckbox>
              </IonItem>
            ))
          }
        </IonList>
        <IonButton expand="block" onClick={ seeData }>Ver información</IonButton>
      </IonContent>
    </IonPage>
  );
}

export default Checkbox;