import { IonButton, IonContent, IonPage, useIonLoading } from '@ionic/react';
import React from 'react';
import Header from '../../components/header/Header';

const Loading: React.FC = () => {
  
  const [ present ] = useIonLoading();

  function showLoading() {
      
    present({
      duration: 2000,
      message: 'Espere por favor ...',
      onDidDismiss: ( $event: any ) => {
        console.log('loading cerrado');
      }
    });
  }
  
  return (
    <IonPage>
      <Header title="loading" />
      <IonContent className="ion-padding" onClick={ showLoading }>
        <IonButton expand="block">Mostrar loading</IonButton>
      </IonContent>
    </IonPage>
  );
}

export default Loading;