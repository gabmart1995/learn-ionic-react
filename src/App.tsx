import { Redirect, Route } from 'react-router-dom';
import { IonApp, IonRouterOutlet, IonSplitPane } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import { Routes } from './routes/Routes';

/* Theme variables */
import './theme/global.css';
import './theme/variables.css';

import React from 'react';
import Menu from './components/menu/Menu';
import { components } from './components/menu/MenuOptions';
import { MenuContext } from './context/MenuContext';


const App: React.FC = () => (
  <IonApp>
    <IonReactRouter>
      <IonSplitPane when="md" contentId="main">

        { /* context */ }
        <MenuContext.Provider value={ components }>
          <Menu />
          <IonRouterOutlet id="main">
            {
              Routes.map(( route: { path: string, component: React.FC  }, index: number ) => (
                <Route exact { ...route } key={ index } />
              ))
            }
            <Route exact path="/">
              <Redirect to="/home" />
            </Route>
          </IonRouterOutlet>
        </MenuContext.Provider>

      </IonSplitPane>
    </IonReactRouter>
  </IonApp>
);

export default App;
