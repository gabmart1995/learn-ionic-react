interface search {
  searchValue: string | undefined,
  property: string,
  array: any[],
  callback: ( value?: any[] ) => void
}

export function searchFilter( value: search ) {

  console.log( value.searchValue );

  if ( value.searchValue!.length === 0 || !value.searchValue ) {
    return value.callback();
  }

  value.searchValue = value.searchValue!.toLowerCase();

  return value.callback(
    value.array.filter(( item: any ) => item[value.property].toLowerCase().includes( value.searchValue! ))
  );
}
