export function fetchApi( domain: string ) {

  // closures : documentacion
  // https://www.youtube.com/watch?v=E6aPTeVujRs

  return function ( resource: string ) {

    let url = domain + resource;

    return {
      get: ( route: string = '', options?: RequestInit ) => {
        return fetch( url + route, options ).then( ( response: Response ) =>  response.json() );
      },
      post: ( route: string = '', options: RequestInit ) => {
        return fetch( url + route, options ).then( ( response: Response ) =>  response.json() );
      },
      put: ( route: string = '', options: RequestInit ) => {
        return fetch( url + route, options ).then( ( response: Response ) =>  response.json() );
      },
      delete: ( route: string = '', options: RequestInit ) => {
        return fetch( url + route, options ).then( ( response: Response ) =>  response.json() );
      }
    };
  }
}

/* const fetchApi = ( domain: string ) => ( resource: string ) => {

  let url = domain + resource;

  return {
    get: ( route: string = '' ) => {
      return fetch( url + route ).then( ( response: Response ) =>  response.json() );
    },
    post: ( route: string = '', options: RequestInit ) => {
      return fetch( url + route, options ).then( ( response: Response ) =>  response.json() );
    },
    put: ( route: string = '', options: RequestInit ) => {
      return fetch( url + route, options ).then( ( response: Response ) =>  response.json() );
    },
    delete: ( route: string = '', options: RequestInit ) => {
      return fetch( url + route, options ).then( ( response: Response ) =>  response.json() );
    }
  };
} */
