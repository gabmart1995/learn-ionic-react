import React from 'react';
import { IonList, IonIcon, IonItem } from '@ionic/react';

import { Component } from '../../interfaces/Component';
import { MenuContext } from '../../context/MenuContext';

const Lists: React.FC = () => {

  function showList( options: Component[] ) {

    return (
      <IonList>
      {
        options.map(( option: Component, index: number ) => (

          <IonItem detail={ true } routerLink={ option.redirectTo } key={ index }>
            <IonIcon color="primary" icon={ option.icon } slot="start"></IonIcon>
            { option.name }
          </IonItem>

        ))
      }
      </IonList>
    );
  }


  return (
    <MenuContext.Consumer>
      { ( options: Component[] ) => showList( options ) }
    </MenuContext.Consumer>
  );

}

export default Lists;
