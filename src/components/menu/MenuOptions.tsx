// iconos
import {
    americanFootballOutline,
    alertCircleOutline,
    beakerOutline,
    radioButtonOffOutline,
    cardOutline,
    checkmarkCircleOutline,
    calendarOutline,
    carOutline,
    gridOutline,
    hammerOutline,
    clipboardOutline,
    listOutline,
    reorderThreeOutline,
    refreshCircleOutline,
    phonePortraitOutline,
    tabletPortraitOutline,
    codeWorkingOutline,
    arrowDownCircleOutline,
    searchOutline,
    copyOutline,
    albumsOutline,
    cogOutline,
    pricetagOutline
  } from 'ionicons/icons';

// interfaces
import { Component } from '../../interfaces/Component';

export const components: Component[] = [
  {  icon: americanFootballOutline, name: 'Action sheet', redirectTo: '/action-sheet' },
  { icon: alertCircleOutline, name: 'Alert', redirectTo: '/alert' },
  { icon: beakerOutline, name: 'Avatar', redirectTo: '/avatar' },
  { icon: radioButtonOffOutline, name: 'Buttons', redirectTo: '/buttons' },
  { icon: cardOutline, name: 'Cards', redirectTo: '/cards' },
  { icon: checkmarkCircleOutline, name: 'Checkbox', redirectTo: '/checkbox' },
  { icon: calendarOutline, name: 'Datetime', redirectTo: '/datetime' },
  { icon: carOutline, name: 'Fabs', redirectTo: '/fab' },
  { icon: gridOutline, name: 'Grid', redirectTo: '/grid' },
  { icon: hammerOutline, name: 'Infinite Scroll', redirectTo: '/infinite-scroll' },
  { icon: clipboardOutline, name: 'Input forms', redirectTo: '/inputs' },
  { icon: listOutline, name: 'Lists - sliding', redirectTo: '/lists' },
  { icon: reorderThreeOutline, name: 'Lists - reorder', redirectTo: '/list-reorder' },
  { icon: refreshCircleOutline, name: 'Loading', redirectTo: '/loading' },
  { icon: phonePortraitOutline, name: 'Modal', redirectTo: '/modal' },
  { icon: tabletPortraitOutline, name: 'Popover', redirectTo: '/popover' },
  { icon: codeWorkingOutline, name: 'Progress', redirectTo: '/progress' },
  { icon: arrowDownCircleOutline, name: 'Refresher', redirectTo: '/refresher' },
  { icon: searchOutline, name: 'Search', redirectTo: '/search' },
  { icon: copyOutline, name: 'Segment', redirectTo: '/segment' },
  { icon: albumsOutline, name: 'Slides', redirectTo: '/slides' },
  { icon: cogOutline, name: 'Tabs', redirectTo: '/tabs/account' },
  { icon: pricetagOutline, name: 'Toasts', redirectTo: '/toasts' }
];
