import { IonMenu, IonHeader, IonToolbar, IonTitle, IonContent, IonList, IonItem, IonLabel, IonIcon, IonMenuToggle } from '@ionic/react';
import React from 'react';

import { Component } from '../../interfaces/Component';
import { MenuContext } from '../../context/MenuContext';

const MenuComponent: React.FC = () => {

  function showMenu( options: Component[] ) {

    return (
      <IonMenu side="start" contentId="main" menuId="first">
        <IonHeader>
          <IonToolbar color="dark">
            <IonTitle>Menú principal</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent>
          <IonList>
            { options.map( ( component: Component, index: number ) => (

                <IonMenuToggle autoHide={ false } key={ index }>
                  <IonItem detail routerLink={ component.redirectTo }>
                    <IonIcon slot="start" icon={ component.icon }></IonIcon>
                    <IonLabel>{ component.name }</IonLabel>
                  </IonItem>
                </IonMenuToggle>
              ))
            }
          </IonList>
        </IonContent>
      </IonMenu>
    );
  }

  return (
    <MenuContext.Consumer>
      { ( value: Component[] ) => showMenu( value )  }
    </MenuContext.Consumer>
  );
}

export default MenuComponent;
