import React from 'react';
import { IonHeader, IonToolbar, IonButtons, IonBackButton, IonTitle  } from '@ionic/react';

const Header: React.FC<{ title: string }> = ({ title }) => (
  <IonHeader>
    <IonToolbar>
      <IonButtons slot="start">
        <IonBackButton color="primary" defaultHref="/"></IonBackButton>
      </IonButtons>
      <IonTitle className="ion-text-capitalize">{ title }</IonTitle>
    </IonToolbar>
  </IonHeader>
);

export default Header;