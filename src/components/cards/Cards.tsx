import React from 'react';
import { 
    IonCard, 
    IonCardHeader, 
    IonCardTitle,
    IonCardContent 
} from '@ionic/react';


interface CardInterface {
    title: string,
    content: string
}

const CardComponent: React.FC<CardInterface> = ( props ) => {

    const { title, content } = props;

    return (
        <IonCard>
            <IonCardHeader>
                <IonCardTitle>{ title }</IonCardTitle>
            </IonCardHeader>
            <IonCardContent>{ content }</IonCardContent>
        </IonCard>
    );
}

export default CardComponent;