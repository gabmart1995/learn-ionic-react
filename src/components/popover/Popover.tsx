import React from 'react';
import { popoverController } from '@ionic/core';
import { IonList, IonLabel, IonItem } from '@ionic/react';

const PopoverInfo: React.FC = () => {

  const items = new Array(7).fill( null );

  function handleClick( valor: number ) {
    console.log( valor );

    popoverController.dismiss({
      item: valor
    });
  }

  return (
    <IonList>
      { items.map(( item: null, index: number ) => (
          <IonItem onClick={ () => handleClick( index + 1 ) } key={ index }>
            <IonLabel>item: { index + 1 }</IonLabel>
          </IonItem>
        ))
      }
    </IonList>
  );
}

export default PopoverInfo;
