import React from 'react';
import { components } from '../components/menu/MenuOptions';

export const MenuContext = React.createContext( components );
